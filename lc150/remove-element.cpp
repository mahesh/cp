#include "../templates/base.hpp"

int removeElement(vector<int> &nums, int val) {
    int i = 0;
    int j = 0;

    while (i < nums.size()) {
        if (nums[j] == val && nums[i] != val) {
            swap(nums[i], nums[j]);
            j++;
        } else if (nums[i] != val) {
            j++;
        }
        i++;
    }
    return j;
}

int main() {
    vector<int> v{0,1,2,2,3,0,4,2};
    cout << removeElement(v, 2) << endl;
    pv(v);
}