#include "../templates/base.hpp"

int maxArea(vector<int> &height) {
    int l = 0;
    int r = height.size() - 1;
    int maxi = 0;
    while (l <= r) {
        int curr = min(height[r], height[l]) * (r - l);
        maxi = max(maxi, curr);
        if (height[l] < height[r]) {
            l++;
        } else {
            r--;
        }
    }
    return maxi;
}

int main() {
    vector<int> v{1, 8, 6, 2, 5, 4, 8, 3, 7};
    cout << maxArea(v) << endl;
}