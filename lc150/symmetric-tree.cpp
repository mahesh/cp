#include "../templates/base.hpp"

bool isSymmetric(TreeNode *root) {
    return isRef(root->left, root->right);
}

bool isRef(TreeNode *left, TreeNode *right) {
    if (left == NULL && right == NULL) {
        return true;
    } else if (left == NULL || right == NULL) {
        return false;
    }
    if (left->val != right->val) {
        return false;
    }
    return isRef(left->left, right->right) && isRef(left->right, right->left);
}

int main() {}