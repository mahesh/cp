#include "../templates/base.hpp"

TreeNode *build(vector<int> in, vector<int> pre, int idx, int pl, int pr) {
    int j = 0;
    while (in[idx] != pre[j + pl]) {
        j++;
    }
    TreeNode* root = new TreeNode(in[idx]);
    root->left = build(in, pre, idx+1, pl, pl+j);
    root->right = build(in, pre, idx+1, pl+j+2, pr);
    return root;
}

int main() {}