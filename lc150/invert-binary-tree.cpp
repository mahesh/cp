#include "../templates/base.hpp"

TreeNode* invert(TreeNode *root) {
    if(root==NULL) {
        return root;
    }
    TreeNode *temp = invert(root->left);
    root->left = invert(root->right);
    root->right = temp;
    return root;
}

int main() {}