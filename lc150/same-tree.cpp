#include "../templates/base.hpp"

bool sameTree(TreeNode *a, TreeNode *b) {
    if (a == NULL && b == NULL) {
        return true;
    } else if (a == NULL || b == NULL) {
        return false;
    }
    if (a->val != b->val) {
        return false;
    }
    return sameTree(a->left, b->left) && sameTree(a->right, b->right);
}

int main() {}