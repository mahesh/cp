#include "../templates/base.hpp"

void merge(vector<int> &v1, int &m, vector<int> &v2, int &n) {
    int z = v1.size()-1;
    while (m >= 0 && n >= 0) {
        pv(v1);
        if (v1[m] > v2[n]) {
            v1[z] = v1[m];
            m--;
        } else {
            v1[z] = v2[n];
            n--;
        }
        z--;
    }
}

int main() {
    vector<int> v1{1,2,3,0,0,0};
    vector<int> v2{2,5,6};
    int m = 2, n = 2;
    merge(v1, m, v2, n);
    pv(v1);
}