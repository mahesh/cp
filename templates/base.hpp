#include <bits/stdc++.h>

using namespace std;

#define pb push_back
#define MOD 1000000007
#define pv(a)             \
    for (auto x : a) {    \
        cout << x << " "; \
    }                     \
    cout << endl;

struct TreeNode {
    int val;
    TreeNode *left;
    TreeNode *right;

    TreeNode() : val(0), left(nullptr), right(nullptr){};
    TreeNode(int v) : val(v), left(nullptr), right(nullptr){};
    TreeNode(int v, TreeNode *left, TreeNode *right) : val(v), left(left), right(right){};
};

struct TreeNodeNext {
    int val;
    TreeNodeNext *left;
    TreeNodeNext *right;
    TreeNodeNext *next;

    TreeNodeNext() : val(0), left(NULL), right(NULL), next(NULL) {}

    TreeNodeNext(int _val) : val(_val), left(NULL), right(NULL), next(NULL) {}

    TreeNodeNext(int _val, TreeNodeNext *_left, TreeNodeNext *_right, TreeNodeNext *_next)
        : val(_val), left(_left), right(_right), next(_next) {}
};